const jwt = require("jsonwebtoken");
//npm install webtoken in gitbash

//usED IN THe algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword
const secret = "CourseBookingAPI"

// [Section] JSON Web Tokens
		/*
		- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
		- Information is kept secure through the use of the secret code
		- Only the system that knows the secret code that can decode the encrypted information
- Imagine JWT as a gift wrapping service that secures the gift with a lock
		- Only the person who knows the secret code can open the lock
		- And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
		- This ensures that the data is secure from the sender to the receiver
		*/

		// Token creation
		/*
		- Analogy
			Pack the gift and provide a lock with the secret code as the key
		*/

module.exports.createAccessToken = (user) => {

//thw data will received from registration
	//When the user logs in, a token will create wit the users info
	//payload of JWT

	const data = {
		id : user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	//the {} butangan dha if gsto ug automatic logout	
	//Generates the token using the data and the secret code with no additional options provided
	//sYNTAX: jwt.sign(payload, secret/prvatekey)
	return jwt.sign(data,secret,{});
}

//Token verification

/*Analogy
-Receive the gify and open the lock to verify if the sender is legitimate and gift was not tampered

*/

module.exports.verify = (req, res,next) =>{
	//the token is retrieved from the request header
	let token = req.headers.authorization;

	if (token !== undefined){
		console.log(token);

		token = token.slice(7, token.length);

		console.log(token);
		return jwt.verify(token, secret, (err,data) =>{
			//if  JWT is not valid
			if (err){
				return res.send({auth:"failed"});
				//if JWT is not valid
			}else {
				//Allows he application to proceed to the next middleware/function in the route
				// will be used as a middleware in the route to verfy the token before proceeding to the function that invokes the controller function
				next();
			}
		})
		// if token does not exist
	}else {
		return res.send ({auth: "failed"})
	}
}

//Token decryption

/*
	Analogy - Open  the gidt and get the content
*/

	module.exports.decode = (token) => {
		if(token !== undefined){ 

		token = token.slice(7, token.length);	
			return jwt.verify(token, secret, (err,data) =>{
		if (err){ 
			return null;
	
			}else{
				// The "decode" method is used to obtain the information from the JWT
				// The "complete:true" option allows us to return additional information from the JWT token
				// Return an object with access to the payload which contains user information stored when the token was generated
				return jwt.decode(token,{complete :true}).payload;
					
			}

		})

	} else {
		return null;
	}
}