const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema 
(
{

	name:{
		type: String,

		//Requires the data for this field to be included when creating a record
		//"true" - defines of the filed  is require or not and the second element is the message that will be printed out in our terminal when the data is present
		required: [true, "Course name is required."]
	},
	description : {
		type: String,
		required: [true, "Description is required."]
	},
	price: {
		type : Number,
		required: [true, "Price is required."]
	},
	isActive: {
		type: Boolean,
		default : true
	},
	createdOn: {
		type : Date,
		//new Date()  intiates a new date that stores the current date and time whenever a course is created in database
	  default: new Date() 
	},
	//we applied the concepy of referencing data 
	enrollees : [{
		userId: {
			type: String,
			required: [true, "UserID is required."]
		},
		enrolledOn:{
			type: Date,
		default : new Date()
		}

	}
	]
}
)
module.exports = mongoose.model("Course", courseSchema);