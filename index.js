const express = require ("express");
const mongoose = require("mongoose");

//it allows our backend application to be available to our front end application
//Cross-Origin Sharing
const cors = require ("cors");

//Allows access to routes defined within our applciation

const userRoutes = require("./routes/userRoutes");
const courseRoutes = require ("./routes/courseRoutes");

const app = express();
//Mongodb Connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.wrqeasd.mongodb.net/b244_booking?retryWrites=true&w=majority",

{
	useNewUrlParser : true,
	useUnifiedTopology: true

});

mongoose.connection.once('open', ()=> console.log('Now connected to the MongoDB Atlas'));

app.use (express.json());
app.use(express.urlencoded({extended:true}));

//defines "/users" to be included for all the users routes deifned in the userRoutes file
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

//WILL use the defined port number for the applciation whenever an environment variable is available or will use the port 4000 id none is defined
//This syntax will allow flexibility when using the application locally or as a hosted application

app.listen (process.env.PORT || 4000, () => {console.log(`API is now online at port ${process.env.PORT || 4000}`)
});

